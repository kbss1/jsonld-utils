export const RDFS_LABEL = 'http://www.w3.org/2000/01/rdf-schema#label';
export const RDFS_COMMENT = 'http://www.w3.org/2000/01/rdf-schema#comment';
export const DEFAULT_HAS_CHILD = "http://onto.fel.cvut.cz/ontologies/cz/cvut/kbss/typeahead/has-child";


/**
 * Gets value of the specified attribute.
 *
 * If the attribute value is a string, it is returned, otherwise a '@value' attribute is retrieved from the nested
 * object.
 * @param obj Object from which the attribute value will be extracted
 * @param att Attribute name
 * @param by (optional) JSON attribute to use instead of '@value' in case the att value is an object
 * @return {*} Attribute value (possibly null)
 */
export function getJsonAttValue(obj, att, by = null) {
    return (obj[att] !== null && obj[att] !== undefined) ? (typeof (obj[att]) !== 'object' ? obj[att] : obj[att][by ? by : '@value']) : null;
}

/**
 * Checks whether the specified JSON-LD object has the specified property value.
 *
 * The property can either have single value, or it can be an array (in which case the value is searched for in the
 * array) or an object with id corresponding to the value.
 * @param object The object to test
 * @param property The property to test
 * @param value The value to look for
 * @return {*|boolean}
 */
export function hasValue(object, property, value) {
    return object[property] !== undefined && object[property] !== null && (object[property] === value || object[property]['@id'] === value
        || (Array.isArray(object[property]) && object[property].indexOf(value) !== -1));
}

/**
 * Gets localized value from the data, if available.
 *
 * If 'data' is an array of objects with language tag and value, value matching the specified locale is
 * returned. If 'data' is an object, its '@value' attribute is returned. If neither case is true, the
 * 'data' itself is returned.
 * @param data Data from which localized value should be extracted
 * @param intl Internationalization object, containing the 'locale' and 'defaultLocale' attributes
 * @return {*}
 */
export function getLocalized(data, intl) {
    const locale = intl.locale,
        defaultLocale = intl.defaultLocale;
    let defaultValue,
        i, len;
    if (!data) {
        return null;
    }
    if (typeof data !== 'object' && !Array.isArray(data)) {
        return data;
    }
    if (!Array.isArray(data)) {
        return data['@value'];
    }

    for (i = 0, len = data.length; i < len; i++) {
        if (data[i]['@language']) {
            if (data[i]['@language'] === locale) {
                return data[i]['@value'];
            } else if (data[i]['@language'] === defaultLocale) {
                defaultValue = data[i]['@value'];
            }
        } else if (!defaultValue) {
            defaultValue = data[i]['@value']
        }
    }
    return defaultValue;
}

/**
 * Transforms JSON-LD (framed) based options list into a list of options suitable for the Typeahead component.
 * @param options The options to process
 * @param intl Object containing locale info
 * @param HAS_CHILD Property representing the child relationship
 */
export function processTypeaheadOptions(options, intl, HAS_CHILD = DEFAULT_HAS_CHILD) {
    if (!options) {
        return [];
    }

    const nodes = {},
        nodesOrdered = [];
    options.forEach(function (item) {
        const result = jsonLdToTypeaheadOption(item, intl, HAS_CHILD);
        nodes[result.id] = result;
        nodesOrdered.push(result);
    });

    // find root...
    const possibleRoots = {};   // Root candidates
    for (let key in nodes) {
        possibleRoots[key] = true;
    }

    // Remove child nodes from the list of root candidates
    for (let key in nodes) {
        const node = nodes[key];
        if (node.childrenIds) {
            for (let i = 0; i < node.childrenIds.length; i++) {
                delete possibleRoots[node.childrenIds[i]];
            }
        }
    }

    // All root candidates are now roots
    let roots = [];
    for (let i = 0, len = nodesOrdered.length; i < len; i++) {
        if (possibleRoots[nodesOrdered[i].id]) {
            roots.push(nodesOrdered[i]);
        }
    }

    // Recursively reconstruct child subtree for each root
    roots = processSubTree(roots.map((root) => root.id), nodes);

    return roots;
}

function processSubTree(childrenIRIs, output) {
    const children = [];
    for (let i in childrenIRIs) {
        const val = output[childrenIRIs[i]];
        if (val.childrenIds) {
            val.children = processSubTree(val.childrenIds, output);
            delete val.childrenIds;
        }
        children.push(val)
    }
    return children;
}

/**
 * Gets the specified JSON-LD object as a simple, more programmatic-friendly object suitable e.g. for typeahead
 * components.
 *
 * The transformation is as follows:
 * <ul>
 *     <li>'@id' -> id</li>
 *     <li>'@type' -> type</li>
 *     <li>rdfs:label -> name</li>
 *     <li>rdfs:comment -> description</li>
 * </ul>
 * @param jsonLd
 * @param intl Object containing locale info
 * @param HAS_CHILD Property representing the child relationship
 */
export function jsonLdToTypeaheadOption(jsonLd, intl, HAS_CHILD = DEFAULT_HAS_CHILD) {
    if (!jsonLd) {
        return null;
    }
    const res = {
        id: jsonLd['@id'],
        type: jsonLd['@type'],
        name: intl ? getLocalized(jsonLd[RDFS_LABEL], intl) : getJsonAttValue(jsonLd, RDFS_LABEL)
    };
    if (jsonLd[RDFS_COMMENT]) {
        res.description = intl ? getLocalized(jsonLd[RDFS_COMMENT], intl) : getJsonAttValue(jsonLd, RDFS_COMMENT);
    }
    if (jsonLd[HAS_CHILD]) {
        res.childrenIds = jsonLd[HAS_CHILD].map((child) => {
            return getJsonAttValue(child, "@id")
        });
        res.children = [];
    }
    return res;
}

/**
 * Transforms the specified JSON-LD input to a list of objects suitable as options for a Select component.
 *
 * This means, that the resulting list consists of objects with value, label and title attributes.
 * @param jsonLd The JSON-LD to process
 * @param intl Object containing locale info
 * @return {*} List of options
 */
export function processSelectOptions(jsonLd, intl) {
    return jsonLd ? jsonLd.map(item => jsonLdToSelectOption(item, intl)) : [];
}

/**
 * Transforms the specified JSON-LD object to an object suitable as option for a select component.
 *
 * @param jsonLd The JSON-LD item to process
 * @param intl Object containing locale info
 */
export function jsonLdToSelectOption(jsonLd, intl) {
    if (!jsonLd) {
        return null;
    }
    const result = {
        value: jsonLd['@id'],
        label: intl ? getLocalized(jsonLd[RDFS_LABEL], intl) : getJsonAttValue(jsonLd, RDFS_LABEL),
        title: intl ? getLocalized(jsonLd[RDFS_COMMENT], intl) : getJsonAttValue(jsonLd, RDFS_COMMENT),
        type: jsonLd['@type'],
        ...jsonLd
    };
    delete result["@id"];
    delete result[RDFS_LABEL];
    delete result[RDFS_COMMENT];
    delete result["@type"];
    return result;
}
