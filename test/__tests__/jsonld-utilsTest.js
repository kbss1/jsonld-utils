import * as JsonLdUtils from "../../src/jsonld-utils";

/**
 * JSON-LD example, framed.
 * @type {*[]}
 */
const JSON_LD = [
    {
        "@id": "http://onto.fel.cvut.cz/ontologies/eccairs-3.4.0.2/vl-a-430/v-1",
        "@type": "http://onto.fel.cvut.cz/ontologies/eccairs/occurrence-category",
        "http://www.w3.org/2000/01/rdf-schema#comment": "Usage Notes:\r\n• This category includes the intentional maneuvering of the aircraft to avoid a collision with terrain, objects/obstacles, weather or other aircraft (Note: The effect of intentional maneuvering is the key consideration).\r\n• Abrupt maneuvering may also result in a loss of control or system/component failure or malfunction. In this case, the event is coded under both categories (e.g., AMAN and Loss of Control–Inflight (LOC–I), AMAN and System/Component Failure or Malfunction (Non- Powerplant) (SCF–NP), or AMAN and System/Component Failure or Malfunction\r\n(Powerplant) (SCF–PP)).\r\n• Abrupt maneuvering may also occur on ground; examples include hard braking maneuver, rapid change of direction to avoid collisions, etc.",
        "http://www.w3.org/2000/01/rdf-schema#label": "1 - AMAN: Abrupt maneuvre"
    },
    {
        "@id": "http://onto.fel.cvut.cz/ontologies/eccairs-3.4.0.2/vl-a-430/v-10",
        "@type": "http://onto.fel.cvut.cz/ontologies/eccairs/occurrence-category",
        "http://www.w3.org/2000/01/rdf-schema#comment": "Usage Notes:\r\n• Includes accumulations that occur inflight or on the ground (i.e., deicing-related).\r\n• Carburetor and induction icing events are coded in the FUEL Related (FUEL) category.\r\n• Windscreen icing which restricts visibility is also covered here.\r\n• Includes ice accumulation on sensors, antennae, and other external surfaces.\r\n• Includes ice accumulation on external surfaces including those directly in front of the engine intakes.",
        "http://www.w3.org/2000/01/rdf-schema#label": "10 - ICE: Icing"
    },
    {
        "@id": "http://onto.fel.cvut.cz/ontologies/eccairs-3.4.0.2/vl-a-430/v-100",
        "@type": "http://onto.fel.cvut.cz/ontologies/eccairs/occurrence-category",
        "http://www.w3.org/2000/01/rdf-schema#comment": "Usage Notes:\r\n• May be used as a precursor to CFIT, LOC-I or LALT.\r\n• Applicable if the pilot was flying according to Visual Flight Rules (VFR), as defined in Annex 2 – Rules of the Air – to the Convention on International Civil Aviation and by any reason found oneself inadvertently in IMC\r\n• Only to be used when loss of visual references is encountered,\r\n• Only to be used if pilot not qualified to fly in IMC and/or aircraft not equipped to fly in IMC",
        "http://www.w3.org/2000/01/rdf-schema#label": "100 - UIMC: Unintended flight in IMC"
    },
    {
        "@id": "http://onto.fel.cvut.cz/ontologies/eccairs-3.4.0.2/vl-a-430/v-101",
        "@type": "http://onto.fel.cvut.cz/ontologies/eccairs/occurrence-category",
        "http://www.w3.org/2000/01/rdf-schema#label": "101 - EXTL: External load related occurrences"
    }
];

const JSON_LD_FOR_TYPEAHEAD_TREE = [
    {
        "@id": "http://onto.fel.cvut.cz/ontologies/eccairs/aviation-3.4.0.2/vl-a-390/v-1000000",
        "@type": [
            "http://onto.fel.cvut.cz/ontologies/eccairs/event-type"
        ],
        "http://onto.fel.cvut.cz/ontologies/eccairs/model/has-child": [
            {"@id": "http://onto.fel.cvut.cz/ontologies/eccairs/aviation-3.4.0.2/vl-a-390/v-4050500"},
            {"@id": "http://onto.fel.cvut.cz/ontologies/eccairs/aviation-3.4.0.2/vl-a-390/v-5020100"}
        ],
        "http://www.w3.org/2000/01/rdf-schema#label": [
            {"@language": "en", "@value": "1000000 - Equipment "}
        ]
    }, {
        "@id": "http://onto.fel.cvut.cz/ontologies/eccairs/aviation-3.4.0.2/vl-a-390/v-4050500",
        "@type": [
            "http://onto.fel.cvut.cz/ontologies/eccairs/event-type"
        ],
        "http://www.w3.org/2000/01/rdf-schema#label": [
            {"@language": "en", "@value": "HMI"}
        ]
    }, {
        "@id": "http://onto.fel.cvut.cz/ontologies/eccairs/aviation-3.4.0.2/vl-a-390/v-5020100",
        "@type": [
            "http://onto.fel.cvut.cz/ontologies/eccairs/event-type"
        ],
        "http://www.w3.org/2000/01/rdf-schema#label": [
            {"@language": "en", "@value": "Aerodrome lighting systems related event 5020100"}
        ]
    }
];

describe('JSON-LD Utils', function () {

    describe('processTypeaheadOptions', () => {
        it('transforms JSON-LD input into Typeahead-friendly format', () => {
            const result = JsonLdUtils.processTypeaheadOptions(JSON_LD);
            expect(result.length).toEqual(JSON_LD.length);
            for (let i = 0, len = JSON_LD.length; i < len; i++) {
                expect(result[i].id).toEqual(JSON_LD[i]['@id']);
                expect(result[i].type).toEqual(JSON_LD[i]['@type']);
                expect(result[i].name).toEqual(JSON_LD[i][JsonLdUtils.RDFS_LABEL]);
                if (JSON_LD[i][JsonLdUtils.RDFS_COMMENT]) {
                    expect(result[i].description).toEqual(JSON_LD[i][JsonLdUtils.RDFS_COMMENT]);
                }
            }
        });

        it('handles transformation of an empty array', () => {
            const result = JsonLdUtils.processTypeaheadOptions([]);
            expect(result).toEqual([]);
        });

        it('handles transformation of null/undefined', () => {
            let result = JsonLdUtils.processTypeaheadOptions(null);
            expect(result).toEqual([]);
            result = JsonLdUtils.processTypeaheadOptions();
            expect(result).toEqual([]);
        });

        it('uses locale to return localized typeahead options from JSON-LD', () => {
            const czLabel = '1 - AMAN: Náhlý manévr',
                czComment = 'Toto je popis',
                jsonLd = [{
                    "@id": "http://onto.fel.cvut.cz/ontologies/eccairs-3.4.0.2/vl-a-430/v-1",
                    "http://www.w3.org/2000/01/rdf-schema#label": [{
                        "@language": "en",
                        "@value": "1 - AMAN: Abrupt maneuvre"
                    }, {
                        "@language": "cz",
                        "@value": czLabel
                    }],
                    "http://www.w3.org/2000/01/rdf-schema#comment": [{
                        "@language": "en",
                        "@value": "And here comes the description"
                    }, {
                        "@language": "cz",
                        "@value": czComment
                    }]
                }], intl = {
                    locale: 'cz'
                },
                result = JsonLdUtils.processTypeaheadOptions(jsonLd, intl);
            expect(result.length).toEqual(1);
            expect(result[0].name).toEqual(czLabel);
            expect(result[0].description).toEqual(czComment);
        });

        it('returns no-language value in case no language tag is specified', () => {
            const jsonLd = [{
                    "@id": "http://onto.fel.cvut.cz/ontologies/eccairs-3.4.0.2/vl-a-430/v-1",
                    "http://www.w3.org/2000/01/rdf-schema#comment": [{
                        "@language": "en",
                        "@value": "Used-for-EN-1"
                    }, {
                        "@value": "Not-used-for-EN-1"
                    }]
                }, {
                    "@id": "http://onto.fel.cvut.cz/ontologies/eccairs-3.4.0.2/vl-a-430/v-2",
                    "http://www.w3.org/2000/01/rdf-schema#label": [{
                        "@value": "Used-for-EN-2"
                    }]
                }], intl = {
                    locale: 'en'
                },
                result = JsonLdUtils.processTypeaheadOptions(jsonLd, intl);

            expect(result.map((child) => {
                return child.description
            })).toContain("Used-for-EN-1");
            expect(result.map((child) => {
                return child.name
            })).toContain("Used-for-EN-2");
        });

        it('returns a tree-like structure for the typeahead component ', () => {
            const jsonLd = JSON_LD_FOR_TYPEAHEAD_TREE,
                intl = {locale: 'en'},
                result = JsonLdUtils.processTypeaheadOptions(jsonLd, intl, "http://onto.fel.cvut.cz/ontologies/eccairs/model/has-child");

            expect(result[0].children.map((child) => {
                return child.id
            })).toContain("http://onto.fel.cvut.cz/ontologies/eccairs/aviation-3.4.0.2/vl-a-390/v-4050500");
            expect(result[0].children.map((child) => {
                return child.name
            })).toContain("HMI");
        });

        it('preserves option ordering', () => {
            const options = [{
                    '@id': '1',
                    'http://www.w3.org/2000/01/rdf-schema#label': '1. value'
                },
                    {
                        '@id': 'before2',
                        'http://www.w3.org/2000/01/rdf-schema#label': 'before2. value'
                    },
                    {
                        '@id': '2',
                        'http://www.w3.org/2000/01/rdf-schema#label': '2. value',
                        'http://onto.fel.cvut.cz/ontologies/form/has-preceding-value': 'before2'
                    },
                    {
                        '@id': '3',
                        'http://www.w3.org/2000/01/rdf-schema#label': '3. value'
                    }],
                intl = {
                    locale: 'en'
                };
            const result = JsonLdUtils.processTypeaheadOptions(options, intl);
            for (let i = 0, len = options.length; i < len; i++) {
                expect(result[i].id).toEqual(options[i]['@id']);
            }
        });
    });

    describe('jsonLdToTypeaheadOption', () => {
        it('returns localized value of the JSON-LD RDFS label as name', () => {
            const czLabel = '1 - AMAN: Náhlý manévr',
                jsonLd = {
                    "@id": "http://onto.fel.cvut.cz/ontologies/eccairs-3.4.0.2/vl-a-430/v-1",
                    "http://www.w3.org/2000/01/rdf-schema#label": [{
                        "@language": "en",
                        "@value": "1 - AMAN: Abrupt maneuvre"
                    }, {
                        "@language": "cz",
                        "@value": czLabel
                    }]
                }, intl = {
                    locale: 'cz'
                },
                result = JsonLdUtils.jsonLdToTypeaheadOption(jsonLd, intl);
            expect(result.name).toEqual(czLabel);
        });

        it('returns localized value of the JSON-LD RDFS comment as description', () => {
            const czComment = '1 - AMAN: Náhlý manévr',
                jsonLd = {
                    "@id": "http://onto.fel.cvut.cz/ontologies/eccairs-3.4.0.2/vl-a-430/v-1",
                    "http://www.w3.org/2000/01/rdf-schema#comment": [{
                        "@language": "en",
                        "@value": "1 - AMAN: Abrupt maneuvre"
                    }, {
                        "@language": "cz",
                        "@value": czComment
                    }]
                }, intl = {
                    locale: 'cz'
                },
                result = JsonLdUtils.jsonLdToTypeaheadOption(jsonLd, intl);
            expect(result.description).toEqual(czComment);
        });
    });

    describe('getJsonAttValue', () => {
        it('extracts value of a JSON literal value', () => {
            const a = 'a',
                b = true,
                c = 12345,
                d = 'Label',
                obj = {
                    'a': a,
                    'b': b,
                    'c': c
                };
            obj[JsonLdUtils.RDFS_LABEL] = d;
            expect(JsonLdUtils.getJsonAttValue(obj, 'a')).toEqual(a);
            expect(JsonLdUtils.getJsonAttValue(obj, 'b')).toEqual(b);
            expect(JsonLdUtils.getJsonAttValue(obj, 'c')).toEqual(c);
            expect(JsonLdUtils.getJsonAttValue(obj, JsonLdUtils.RDFS_LABEL)).toEqual(d);
        });

        it('extracts value from a JSON value object with tag', () => {
            const label = 'Label',
                obj = {};
            obj[JsonLdUtils.RDFS_LABEL] = {
                '@language': 'en',
                '@value': label
            };
            expect(JsonLdUtils.getJsonAttValue(obj, JsonLdUtils.RDFS_LABEL)).toEqual(label);
        });

        it('returns null if the attribute is not present', () => {
            const obj = {};
            obj[JsonLdUtils.RDFS_LABEL] = {
                '@language': 'en',
                '@value': 'Label'
            };
            expect(JsonLdUtils.getJsonAttValue(obj, JsonLdUtils.RDFS_COMMENT)).toBeNull();
        });

        it('extracts value using the passed in \'by\' attribute', () => {
            const obj = {}, att = 'http://onto.fel.cvut.cz/ontologies/form/has-question-origin',
                value = 'http://onto.fel.cvut.cz/ontologies/123346';
            obj[att] = {
                '@id': value
            };
            expect(JsonLdUtils.getJsonAttValue(obj, att, '@id')).toEqual(value);
        });

        it('extracts boolean literal value', () => {
            const obj = {
                "http://ahoj": {
                    "@value": true
                }
            };
            expect(JsonLdUtils.getJsonAttValue(obj, 'http://ahoj')).toBeTruthy();
        });
    });

    describe('hasValue', () => {
        it('returns true for literal value of attribute', () => {
            const obj = {}, att = 'http://onto.fel.cvut.cz/ontologies/form/has-question-origin', value = 117;
            obj[att] = value;
            expect(JsonLdUtils.hasValue(obj, att, value)).toBeTruthy();
        });

        it('returns true for value in property array', () => {
            const obj = {}, att = 'http://onto.fel.cvut.cz/ontologies/form/has-question-origin', value = 117;
            obj[att] = [1, 2, 3, 4, 5, value];
            expect(JsonLdUtils.hasValue(obj, att, value)).toBeTruthy();
        });

        it('returns true for object with @id attribute equal to expected value', () => {
            const obj = {}, att = 'http://onto.fel.cvut.cz/ontologies/form/has-question-origin',
                value = 'http://onto.fel.cvut.cz/ontologies/123346';
            obj[att] = {
                '@id': value
            };
            expect(JsonLdUtils.hasValue(obj, att, value)).toBeTruthy();
        });

        it('returns false for different property value', () => {
            const obj = {}, att = 'http://onto.fel.cvut.cz/ontologies/form/has-question-origin', value = 117;
            obj[att] = 12345;
            expect(JsonLdUtils.hasValue(obj, att, value)).toBeFalsy();
        });

        it('returns false for property not present', () => {
            const obj = {}, att = 'http://onto.fel.cvut.cz/ontologies/form/has-question-origin', value = 117;
            expect(JsonLdUtils.hasValue(obj, att, value)).toBeFalsy();
        });

        it("return false for property value which contains expected attribute name as substring", () => {
            const obj = {}, att = 'http://onto.fel.cvut.cz/ontologies/form/has-value';
            obj[att] = "datetime";
            expect(JsonLdUtils.hasValue(obj, att, "date")).toBeFalsy();
        });
    });

    describe('jsonLdToSelectOption', () => {
        it('generates an object suitable for select option generation from a JSON-LD object', () => {
            const jsonLd = JSON_LD[0],
                result = JsonLdUtils.jsonLdToSelectOption(jsonLd);
            expect(result.value).toEqual(jsonLd['@id']);
            expect(result.label).toEqual(jsonLd[JsonLdUtils.RDFS_LABEL]);
            expect(result.title).toEqual(jsonLd[JsonLdUtils.RDFS_COMMENT]);
        });

        it('returns null from null argument', () => {
            expect(JsonLdUtils.jsonLdToSelectOption(null)).toBeNull();
        });

        it('returns localized value of the JSON-LD RDFS label as label', () => {
            const czLabel = '1 - AMAN: Náhlý manévr',
                jsonLd = {
                    "@id": "http://onto.fel.cvut.cz/ontologies/eccairs-3.4.0.2/vl-a-430/v-1",
                    "http://www.w3.org/2000/01/rdf-schema#label": [{
                        "@language": "en",
                        "@value": "1 - AMAN: Abrupt maneuvre"
                    }, {
                        "@language": "cz",
                        "@value": czLabel
                    }]
                }, intl = {
                    locale: 'cz'
                },
                result = JsonLdUtils.jsonLdToSelectOption(jsonLd, intl);
            expect(result.label).toEqual(czLabel);
        });

        it('returns option with localized value of the JSON-LD RDFS comment as title', () => {
            const czComment = '1 - AMAN: Náhlý manévr',
                jsonLd = {
                    "@id": "http://onto.fel.cvut.cz/ontologies/eccairs-3.4.0.2/vl-a-430/v-1",
                    "http://www.w3.org/2000/01/rdf-schema#comment": [{
                        "@language": "en",
                        "@value": "1 - AMAN: Abrupt maneuvre"
                    }, {
                        "@language": "cz",
                        "@value": czComment
                    }]
                }, intl = {
                    locale: 'cz'
                },
                result = JsonLdUtils.jsonLdToSelectOption(jsonLd, intl);
            expect(result.title).toEqual(czComment);
        });

        it('returns option with @type property value as type attribute', () => {
            const jsonLd = JSON_LD_FOR_TYPEAHEAD_TREE[0];
            const result = JsonLdUtils.jsonLdToSelectOption(jsonLd, {locale: 'en'});
            expect(result.type).toEqual(jsonLd['@type']);
        });

        it("preserves all other attributes of argument", () => {
            const input = {
                "@id": "http://onto.fel.cvut.cz/ontologies/eccairs-3.4.0.2/vl-a-430/v-1",
                "http://www.w3.org/2000/01/rdf-schema#comment": {
                    "@language": "en",
                    "@value": "1 - AMAN: Abrupt maneuvre"
                },
                "https://schema.org/position": 117
            };
            const result = JsonLdUtils.jsonLdToSelectOption(input);
            expect(result["https://schema.org/position"]).toEqual(input["https://schema.org/position"]);
        });
    });

    describe('getLocalized', () => {

        const data = [{
            "@language": "en",
            "@value": "Not processed"
        }, {
            "@language": "cz",
            "@value": "Nezpracováno"
        }];
        let intl;

        beforeEach(() => {
            intl = {
                defaultLocale: 'en'
            }
        });

        it('extracts localized version of label', () => {
            intl.locale = 'cz';

            const result = JsonLdUtils.getLocalized(data, intl);
            expect(result).toEqual(data[1]['@value']);
        });

        it('falls back to default locale when data does not support current locale', () => {
            intl.locale = 'sp';

            const result = JsonLdUtils.getLocalized(data, intl);
            expect(result).toEqual(data[0]['@value']);
        });

        it('returns null for null/undefined data passed in', () => {
            expect(JsonLdUtils.getLocalized(null, intl)).toBeNull();
            expect(JsonLdUtils.getLocalized(undefined, intl)).toBeNull();
        });
    });
});
