# JSON-LD Utils

Helper functions for processing JSON-LD.


## Change log

### 0.1.2
- Make `jsonLdToSelectOption` preserve attributes (except those that are mapped to value, label, comment and type).

### 0.1.1
- Refactor the exported class with static methods to exporting individual functions.

### 0.1.0
- Revamp to Webpack-based build and structure, update dependencies.

#### 0.0.7
- support for has-child relationship for the typeahead component.

